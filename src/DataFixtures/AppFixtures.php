<?php

namespace App\DataFixtures;

use App\Entity\Blog;
use App\Entity\Etat;
use App\Entity\News;
use App\Entity\User;
use App\Entity\Alike;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {

         /////////////////SUPRESSION FICHIER D'UN DOSSIER///////////////
        //  $dossier_traite = "public/images/articles";
        //  $repertoire = opendir($dossier_traite);      
        //  while (false !== ($fichier = readdir($repertoire))) {
        //      $chemin = $dossier_traite."/".$fichier;
        //      if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier)){
        //          unlink($chemin); 
        //      }
        //  }
        //  closedir($repertoire); 
         ////////////////////////////////////////////////////////////////

        //Blog
        $blog = new Blog();
        $blog->setSlogan("Bienvenue sur mon Blog Cinéma");
        $manager->persist($blog);

        $faker = \Faker\Factory::create("fr_FR");

        //USER
        $user1=new User();
        $user1->setUsername('auteur1')
             ->setEmail('auteur1@gmail.com')
             ->setRoles(['ROLE_USER'])
             ->setCode('')
             ->setPassword($this->encoder->encodePassword($user1,'12345678'));
        $manager->persist($user1);

        $user2=new User();
        $user2->setUsername('auteur2')
             ->setEmail('auteur2@gmail.com')
             ->setRoles(['ROLE_USER'])
             ->setCode('')
             ->setPassword($this->encoder->encodePassword($user2,'12345678'));
        $manager->persist($user2);

        $admin=new User();
        $admin->setUsername('admin')
             ->setEmail('admin@gmail.com')
             ->setRoles(['ROLE_USER','ROLE_ADMIN'])
             ->setCode('')
             ->setPassword($this->encoder->encodePassword($admin,'12345678'));
        $manager->persist($admin);

        //CATEGORY
        $category1=new Category();
        $category1->setTitle('Thriller')
                ->setDescription('Film d\'enquête policière avec un grand suspense ');
        $manager->persist($category1);

        $category2=new Category();
        $category2->setTitle('Histoire')
                ->setDescription('Film se situant dans un contexte historique');
        $manager->persist($category2);

        $category3=new Category();
        $category3->setTitle('Comédie')
                ->setDescription('Film basé sur l\'humour et la joie de vivre ');
        $manager->persist($category3);

        //ETAT
        $etat1=new Etat();
        $etat1->setStatut('Attente');
        $manager->persist($etat1);

        $etat2=new Etat();
        $etat2->setStatut('Brouillon');
        $manager->persist($etat2);
        
        $etat3=new Etat();
        $etat3->setStatut('Publié');
        $manager->persist($etat3);

        //NEWS
        $news1=new News();
        $contenu='<p style="text-align: center;"><span style="font-size: 18pt;"><strong>James Bond : Mourir peut attendre repouss&eacute; &agrave; 2021 ?</strong></span></p>';
        $contenu.='<p>&nbsp;</p>';
        $contenu.='<p style="text-align: center;">Le 25&egrave;me James Bond "Mourir peut attendre", pr&eacute;vu pour le 11 novembre prochain, pourrait encore &ecirc;tre d&eacute;cal&eacute; et ne sortir qu\'en 2021.</p>';
        $contenu.='<p style="text-align: center;">&nbsp;</p>';
        $contenu.='<p style="text-align: center;"><a title="Mourir peut attendre (U.S.A.,Grande-Bretagne) - 2020 (Action,Thriller,Espionnage)" href="http://www.allocine.fr/film/fichefilm_gen_cfilm=212358.html">Mourir peut attendre</a>&nbsp;va-t-il encore... attendre ? C\'est la question que se posent apparemment certains ex&eacute;cutifs de chez Universal. Apr&egrave;s son report d\'avril &agrave; novembre 2020 suite &agrave; la fermeture des salles &agrave; cause de l\'&eacute;pid&eacute;mie de COVID-19, la nouvelle aventure de James Bond va-t-elle &agrave; nouveau &ecirc;tre d&eacute;cal&eacute;e ?</p>';
        $contenu.='<p style="text-align: center;">&nbsp;</p>';
        $contenu.='<p style="text-align: center;"><img src="../../images/news/jamesbond.jpg" alt="" width="183" height="275" /></p>';
        $news1->setContent($contenu);
        $news1->setActif(1);
        $news1->setCreatedAt(new \DateTime());
        $manager->persist( $news1);

        $news2=new News();
        $contenu='<h1 class="titre " style="text-align: center;"><span style="font-size: 24pt;">&nbsp;Festival du cin&eacute;ma am&eacute;ricain de Deauville&nbsp;</span></h1>';
        $contenu.='<h1 class="titre " style="text-align: center;"><span style="font-size: 14pt;">ENTRETIEN : &laquo; Nous souhaitons organiser ce festival &raquo;</span></h1>';
        $contenu.='<p><span style="font-size: 14pt;">Les organisateurs du festival du cin&eacute;ma am&eacute;ricain de Deauville (Calvados) feront tout pour que ce grand rendez-vous ait lieu, comme pr&eacute;vu, du 4 au 13 septembre 2020, en tenant compte de l&rsquo;&eacute;volution du coronavirus. Comme des retrouvailles entre public et cin&eacute;ma. C&rsquo;est ce qu&rsquo;explique Bruno Barde, directeur artistique du festival.</span></p>';
        $contenu.='<p style="text-align: center;">&nbsp;</p>';
        $contenu.='<p style="text-align: center;"><img src="../../images/news/festival.jpg" alt="" width="183" height="275" /></p>';
        $news2->setContent($contenu);
        $news2->setActif(1);
        $news2->setCreatedAt(new \DateTime());
        $manager->persist( $news2);

        //Articles
        for ($i=1; $i <=15; $i++) { 
            
            $article=new Article();   

            $content='<p>'.$faker->paragraph().'</p>'; 
            $content.='<p>'.$faker->paragraph().'</p>'; 
            $content.='<p>'.$faker->paragraph().'</p>'; 

            $numuser=($i%2)+1;
           
            /////////////////////Download des images///////////////////////////
            // $url="https://picsum.photos/id/".(1000+$i)."/350/".(250+$i).".jpg";
            // $this->downloadUrl($url,"public/images/articles/");
            ///////////////////////////////////////////////////////////////////
            
            $article->setTitle($faker->sentence())
                    ->setUser(${"user".$numuser})
                    ->setContent($content)
                    ->setImage((250+$i).".jpg")
                    ->setCreatedAt($faker->dateTimeBetween('-6 months'));
            
            if($i>=10){
                $article->setCategory($category3);
            }elseif($i>=5) {
                $article->setCategory($category2);
            }else{
                $article->setCategory($category1);
            }

            if($i>=13){
                $article->setEtat($etat1);
            }elseif($i>=10) {
                $article->setEtat($etat2);
            }else{
                $article->setEtat($etat3);
            }

            $manager->persist( $article);

            //Ajout likes
            $like=new Alike();
            $like->setUser($user1);
            $like->setArticle($article);
            $manager->persist($like);

            $like=new Alike();
            $like->setUser($user2);
            $like->setArticle($article);
            $manager->persist($like);
            
            //Ajout Commentaires
            $nbCommentaires=mt_rand(4,10);
            for ($j=0; $j < $nbCommentaires; $j++) { 
                
                $comment=new Comment();

                $numuser=($j%2)+1;

                $content=implode(" - ",$faker->paragraphs(2));
                $now=new \DateTime();
                $interval=$now->diff($article->getCreatedAt());
                $days=$interval->days;

                $comment->setUser(${"user".$numuser})
                        ->setContent($content)
                        ->setCreatedAt($faker->dateTimeBetween('-'.$days.' days'))
                        ->setArticle($article);

                $manager->persist( $comment);
            }
        }       

        $manager->flush();

    }

    ////////////////////////////Fonctions download images/////////////////////////////
    public function downloadUrl($url,$chemin){
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        $result = curl_exec($curl);
        if ($result !== false) {
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);          
            if ($statusCode == 404) return false;            
        }

        $filename = basename(parse_url($url, PHP_URL_PATH));
        file_put_contents($chemin.$filename, file_get_contents($url));
        
    }
    ///////////////////////////////////////////////////////////////////////////////

}
