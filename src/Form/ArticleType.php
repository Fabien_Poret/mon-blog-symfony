<?php

namespace App\Form;

use App\Entity\Etat;
use App\Entity\Article;
use App\Entity\Category;
use App\Repository\EtatRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $user=$options['currentUser'];
        //dump($user);

        $builder
            ->add('title')
            //->add('category')
            //  ->add('category',EntityType::class,[
            //     'class'=>Category::class,
            //     'choice_label' => 'title',
            //     'placeholder' => 'Choix Catégorie',
            // ])
            //->add('etat')
            ->add('content')
            //->add('image')
            //->add('createdAt')
            ->add('fichierimage', FileType::class, [
                'mapped' => false,
                'label' => 'Upload fichier',
                'required' => false,
                // Les champs non mappés ne peuvent pas utiliser les annotations pour les validations
                // dans les entités associées, nous devons donc utiliser des contraintes de classe
                // en utilisant le composant  Symfony\Component\Validator\Constraints\File;
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])

        ;

        //Affichage de tous les états si admin sinon on n'affiche pas publié
        $isRoleAdmin=in_array('ROLE_ADMIN',$user->getRoles());

        if($isRoleAdmin){
            $builder 
            ->add('etat',EntityType::class,[
                'class'=>Etat::class,
                'choice_label'=>'statut',
                'placeholder' => 'Choix Etat',
            ]) ; 
        }else{
            $builder 
            ->add('etat',EntityType::class,[
                'class'=>Etat::class,
                'choice_label'=>'statut',
                //'placeholder' => 'Choix Etat',
                'query_builder' => function (EtatRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->Where("e.statut<>'Publié'")
                        ->orderBy('e.statut', 'ASC');
                },
            ]);  
        }


        //POUR AUTOCOMPLETION
        //Création de deux champs de type text et hidden non mappé qui affiche 
        //l'Id et le title de la catégroy lorsqu'on édite un article et qui laisse les champs vides
        //lorsque l'on en crée un
        $categoryId='';
        $categoryTitle='';
        $article=$builder->getData();       
        if($article->getId()){
            $categoryId=$article->getCategory()->getId();
            $categoryTitle=$article->getCategory()->getTitle();
        }

        $builder->add('category',HiddenType::class,[
            'mapped' => false,
            'required' => true,
            'data' => $categoryId,
        ])//l'attribut id sera "article_category"
        ->add('category_auto',TextType::class,[
            'mapped' => false,
            'label' => 'Catégorie',
            'required' => true,
            'data' => $categoryTitle,
        ]);//l'attribut id sera "article_category_auto"



        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'currentUser'=>null
        ]);
    }
}
