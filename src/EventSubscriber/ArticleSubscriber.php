<?php

namespace App\EventSubscriber;
 
use App\Events\ArticleEditEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
 
class ArticleSubscriber implements EventSubscriberInterface
{

  private $mailer;
  private $urlGenerator;
 
  //Récupération d'un objet de type Swift_Mailer par injection de dépendance
  //au moment de l'instanciation du Subscriber (abonné)
  //et d'un générateur d'url
  public function __construct( \Swift_Mailer $mailer, UrlGeneratorInterface $urlGenerator)
     {
         $this->mailer = $mailer;
         $this->urlGenerator = $urlGenerator;
     }
 

   //Inscription de l'évenement ArticleEditEvent sur ce Subscriber (abonné)
    public static function getSubscribedEvents()
    {
      return [
        ArticleEditEvent::class => 'onArticleEdit',
      ];
    }
    //Récupération des infos de l'objet article à partir de l'objet event
    public function onArticleEdit(ArticleEditEvent $event)
    {
      
      $article=$event->getArticle();

      if($article->getEtat()->getStatut()=='Attente'){

        $href = $this->urlGenerator->generate('article_show', [
          'id' => $article->getId()
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $lien='<br><a href="'.$href.'">
        Afficher Article
        </a>';

        //Envoi du message
        $message = new \Swift_Message('NOUVEL ARTICLE');
        $message ->setFrom('admin.blog@email.fr');
        $message ->setTo(['admin.blog@email.fr'=> 'admin']);
        $message ->setBody("
                    Un Nouvel article a été modifié 
                    <br>Titre de l'article <strong>".$article->getTitle()."</strong>
                    <br>par <strong>".$article->getUser()."</strong>
                    <br>Lien vers l'article : 
                    ".$lien,
                'text/html'
        );
        try {
            $retour=$this->mailer->send($message);
        }
        catch (\Swift_TransportException $e) {
            $retour= $e->getMessage();
        }
      }

    }
}
