<?php

namespace App\EventSubscriber;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class LoginSubscriber implements EventSubscriberInterface
{

    //Je déclare 3 variables locales 
    private $manager;
    private $userRepo;
    private $session;

    public function __construct(SessionInterface $session,EntityManagerInterface $manager,UserRepository $userRepo){
        //Je charge les 3 variables locales par injection de dépendance
        $this->manager=$manager; // Un manager pour modifier la date de connexion du user
        $this->userRepo=$userRepo;// Un repository pour récupérer le user qui vient de se connecter
        $this->session=$session;// Une session pour stocker la date heure de dernière connexion et la récupérer dans le twig base
    }

    public function onSecurityAuthenticationSuccess(AuthenticationEvent $event)
    {
        
        //Je récupère le nom du user connecté (seul élément que je peux récupérer à partir de $event)
        //$username=$event->getAuthenticationToken()->getUsername();
        $user=$event->getAuthenticationToken()->getUser();
        //Je récupère le user à partir de son nom
        //$user=$this->userRepo->findOneByUsername($username);
       
        //Si il existe
        if($user!== "anon."){
            //Je charge dans la session une variable avec la date de la connexion précédente
            $this->session->set('lastConnect', $user->getConnectedAt());
            //Je modifie la date de dernière connexion
            $user->setConnectedAt(new \DateTime());
            //Je sauvegarde les données du user
            $this->manager->persist($user);
            $this->manager->flush();
        }
        
    }

    public static function getSubscribedEvents()
    {
        return [
            'security.authentication.success' => 'onSecurityAuthenticationSuccess',
        ];
    }
}
