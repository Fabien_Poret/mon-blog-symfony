<?php

namespace App\EventSubscriber;
 
use App\Events\CommentCreatedEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
 
class CommentSubscriber implements EventSubscriberInterface
{

    private $mailer;
    private $urlGenerator;

    //Récupération d'un objet de type Swift_Mailer par injection de dépendance
    //Récupération d'un objet de type UrlGeneratorInterface par injection de dépendance
    //au moment de l'instanciation du Subscriber (abonné)
    public function __construct( \Swift_Mailer $mailer, UrlGeneratorInterface $urlGenerator)
    {
        $this->mailer = $mailer;
        $this->urlGenerator = $urlGenerator;
    }

    //Inscription de l'évenement CommentCreatedEvent sur ce Subscriber (abonné)
    public static function getSubscribedEvents()
    {
      return [
        CommentCreatedEvent::class => 'onCreated',
      ];
    }
 
    public function onCreated(CommentCreatedEvent $event)
    {
    
        //Récupération des infos de l'objet comment à partir de l'objet event
        $texte=$event->getComment()->getContent();
        $commentateur=$event->getComment()->getUser()->getUsername();
        $titre=$event->getComment()->getArticle()->getTitle();
        $auteur=$event->getComment()->getArticle()->getUser()->getUsername();

        ///////////////////////AJOUT LIENS////////////////////////////
        $href1 = $this->urlGenerator->generate('comment_invalide', [
            'code' => $event->getComment()->getCode()
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        $href2 = $this->urlGenerator->generate('comment_valide', [
            'code' => $event->getComment()->getCode()
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        $lien='<br><a href="'.$href1.'">
                    Suppression du commentaire
                </a>
                <br><a href="'.$href2.'">
                    Validation du commentaire
                </a>';
        $texte.='<br>'.$lien;
        ///////////////////////////////////////////////////////////////
        
        //Envoi du message
        $message = new \Swift_Message('NOUVEAU COMMENTAIRE');
        $message ->setFrom('admin.blog@email.fr');
        $message ->setTo(['admin.blog@email.fr'=> 'admin']);
        $message ->setBody("
                    Commentaire de ".$commentateur." sur l'article ".$titre." de ".$auteur."
                    <br><br>
                    ".$texte."
                    ",
                'text/html'
        );
        try {
            $retour=$this->mailer->send($message);
        }
        catch (\Swift_TransportException $e) {
            $retour= $e->getMessage();
        }
    }
}
