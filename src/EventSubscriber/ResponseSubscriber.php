<?php

namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ResponseSubscriber implements EventSubscriberInterface
{
    public function onResponseEvent(ResponseEvent $event)
    {
        //Récupération des infos de l'objet response à partir de l'objet event             
        $response=$event->getResponse();
        //Récupération du contenu de l’objet response
        $content = $response->getContent();
        $html = '<div style="background:orange; width:100%; text-align:center;">Maintenance en cours</div>';
        //Ajout d’un élément html au contenu de l’objet response
        $content = str_replace(
            '<body>',
            '<body> '.$html,
            $content
        ); 
        // Modification du contenu de la réponse  
        $response->setContent($content);
        
        return $response;

    }

    // public function onRequestEvent(RequestEvent $event)
    // {       
    //    dump($event);
    // }


    public static function getSubscribedEvents()
    {
        return [
            //ResponseEvent::class => 'onResponseEvent',
            //RequestEvent::class => 'onRequestEvent',
        ];
    }
}
