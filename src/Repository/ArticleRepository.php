<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }


    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /////////////////////////////////////////////////////////////////////////
    //////////////////////////////QUERYBUILDER///////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    

    //QUERYBUILDER SANS PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function findArticlePubliesQB()
    {
        $qb= $this->createQueryBuilder('article')
            ->innerJoin('article.etat', 'etat')
            ->Where("etat.statut='Publié'")
            ->orderBy('article.id', 'DESC')
            ->getQuery()
        ;

        //dump( $qb->getDql());

        return $qb->getResult();

    }

    //QUERYBUILDER AVEC PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function findArticleQB($value)
    {
        $qb= $this->createQueryBuilder('article')
            ->innerJoin('article.etat', 'etat')
            ->Where("etat.statut=:val")
            ->setParameter('val', $value)
            ->orderBy('article.id', 'DESC')
            ->getQuery()
        ;

        //dump( $qb->getDql());

        return $qb->getResult();

    }

    /////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////DQL/////////////////////////////////
    /////////////////////////////////////////////////////////////////////////

     //DQL SANS PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function findArticlePubliesDQL(): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT article 
            FROM App\Entity\Article article 
            INNER JOIN article.etat etat 
            WHERE etat.statut = 'Publié' 
            ORDER BY article.id DESC "
        );

        //dump($query->getSQL());

        return $query->getResult();
    }

    //DQL AVEC PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function findArticleDQL($value): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT article 
            FROM App\Entity\Article article 
            INNER JOIN article.etat etat 
            WHERE etat.statut = :statut 
            ORDER BY article.id DESC "
        )->setParameter('statut', $value );

        return $query->getResult();
    }


    /////////////////////////////////////////////////////////////////////////
    //////////////////////////////////SQLNATIVE///////////////////////////////
    /////////////////////////////////////////////////////////////////////////

    //SQLNATIVE SANS PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function findArticlePubliesSQLNATIVE(): array
    {
        $entityManager = $this->getEntityManager();

        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Article', 'a');
    
        $query = $entityManager->createNativeQuery( "
            SELECT article.* 
            FROM  article 
            INNER JOIN  etat 
            ON article.etat_id=etat.id
            WHERE etat.statut = 'Publié'
            ORDER BY article.id DESC ",
            $rsm
        );

        return $query->getResult();
    }

    //SQLNATIVE AVEC PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function findArticleSQLNATIVE($value): array
    {
        $entityManager = $this->getEntityManager();

        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Article', 'a');
    
        $query = $entityManager->createNativeQuery( "
            SELECT article.* 
            FROM  article 
            INNER JOIN  etat 
            ON article.etat_id=etat.id
            WHERE etat.statut = ? 
            ORDER BY article.id DESC ",
            $rsm
        );

        $query->setParameter(1, $value);

        return $query->getResult();
    }

    /////////////////////////////////////////////////////////////////////////
    //////////////////////////////////SQL - PDO///////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    
     //SQL - PDO  SANS PARAMETRES
    /**
     * @return Object[] Returns an array of  objects
     */
    public function find3BestArticlePubliesSQL() : array
    {
        //Récupération de la connexion à la base 
        $conn = $this->getEntityManager()->getConnection();
        //Code SQL
         $sql ="
        SELECT article.id,
        article.title,
        article.image,
        COUNT(alike.id) AS Count_like
        FROM (article article
        INNER JOIN etat etat ON (article.etat_id = etat.id))
        INNER JOIN alike alike ON (alike.article_id = article.id)
        WHERE (etat.statut = 'Publié') 
        GROUP BY article.id, article.title, article.image
        ORDER BY COUNT(alike.id)  DESC, article.id DESC 
        LIMIT 3
            ";
         //Préparation de la requête
          $stmt = $conn->prepare($sql);
          //Paramètres passés sous forme de tableau associatif
          // par exemple ici WHERE (etat.statut = :statut) 
          //$stmt->execute(['statut' =>$value]);
          //Exécution de la requête
          $stmt->execute();
          // renvoie un tableau d'objets anonymes 
         return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    //SQL - PDO AVEC PARAMETRES
    /**
     * @return Object[] Returns an array of  objects
     */
    public function find3BestArticleSQL($value) : array
    {
        //Récupération de la connexion à la base 
        $conn = $this->getEntityManager()->getConnection();
        //Code SQL
         $sql ="
        SELECT article.id,
        article.title,
        article.image,
        COUNT(alike.id) AS Count_like
        FROM (article article
        INNER JOIN etat etat ON (article.etat_id = etat.id))
        INNER JOIN alike alike ON (alike.article_id = article.id)
        WHERE (etat.statut = :statut) 
        GROUP BY article.id, article.title, article.image
        ORDER BY COUNT(alike.id)  DESC, article.id DESC
        LIMIT 3
            ";
         //Préparation de la requête
          $stmt = $conn->prepare($sql);
          //Paramètres passés sous forme de tableau associatif
          // par exemple ici WHERE (etat.statut = :statut) 
          $stmt->execute(['statut' =>$value]);
          //Exécution de la requête
          $stmt->execute();
          // renvoie un tableau d'objets anonymes 
         return $stmt->fetchAll(\PDO::FETCH_OBJ);
    }

    ////////FONCTION QUI VA CHERCHER LES 3 DERNIERS ARTICLES PUBLIES//////.
    /**
     * @return Article[] Returns an array of Article objects
     */
    
    public function findLastArticles($etat): array
    {
        $entityManager = $this->getEntityManager();

        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Article', 'a');
    
        $query = $entityManager->createNativeQuery( "
            SELECT article.* 
            FROM  article 
            INNER JOIN  etat 
            ON article.etat_id=etat.id
            WHERE etat.statut = ? 
            ORDER BY article.id DESC 
            Limit 3
            ",
            $rsm
        );
        $query->setParameter(1, $etat);

        return $query->getResult();
    }


   //FONCTION QUI VA CHERCHER LES ARTICLES DONT DES COMMENTAIRES NE SONT PAS ENCORE VALIDES /
   //SI UN USER_ID EST PASSE EN PARAMETRE ON NE RECUPERE QUE LES COMMENTAIRES DE CE USER
    /**
     * @return Article[] Returns an array of Article objects
     */
    
    public function findArticleWithInvalidComment($userId=null): array
    {
        $entityManager = $this->getEntityManager();

        $rsm2 = new ResultSetMappingBuilder($entityManager);
        $rsm2->addRootEntityFromClassMetadata('App\Entity\Article', 'a');

        $sql=" SELECT article.*
        FROM comment comment
             INNER JOIN article article
                ON (comment.article_id = article.id)
        WHERE (comment.code <> '') AND (comment.code IS NOT NULL)";

        if ($userId) {
            $sql.=" AND comment.user_id=?";
        }
        

       // $sql.=" GROUP BY article.id, article.title ";
    
        $query = $entityManager->createNativeQuery(
            $sql,
            $rsm2
        );

        if ($userId) {
            $query->setParameter(1, $userId);
        }
        

        return $query->getResult();
    }

    //Fonction pour autocomplete : liste les articles publiés triés par ordre alphabétique croissant SQL - PDO 
    public function findAutoCompleteTitle() : array
    {
        //Récupération de la connexion à la base 
        $conn = $this->getEntityManager()->getConnection();
        //Code SQL
        $sql ="select article.id as value,article.title  as label from article INNER JOIN etat ON etat.id=article.etat_id WHERE etat.statut='Publié' ORDER BY article.title";
        //Préparation de la requête
        $stmt = $conn->prepare($sql);
        //Exécution de la requête
        $stmt->execute();
        // renvoie un tableau d'objets anonymes 
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
}
