<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

     //Fonction pour autocomplete : liste les catégories  triés par ordre alphabétique croissant SQL - PDO 
    public function findAutoCompleteCategory() : array
    {
        //Récupération de la connexion à la base 
        $conn = $this->getEntityManager()->getConnection();
        //Code SQL
         $sql ="select category.id as value,category.title  as label from category ORDER BY category.title";
         //Préparation de la requête
          $stmt = $conn->prepare($sql);
          //Exécution de la requête
          $stmt->execute();
          // renvoie un tableau d'objets anonymes 
         return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
