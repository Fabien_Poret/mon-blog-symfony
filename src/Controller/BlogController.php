<?php

namespace App\Controller;

use App\Repository\BlogRepository;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(Request $request,ArticleRepository $articleRepository,BlogRepository $blogRepository,SessionInterface $session)
    {

        //dump($_ENV["mail_admin"]);

        /////////Récupération d'un cookie ne fonctionne pas hcez tous les hébergeurs
        $articleLu=null;
        //if($this->getUser()){
        //    $IdArticleLu=$request->cookies->get('articleLu_'.$this->getUser()->getId(),'');
        //    $articleLu=$articleRepository->find($IdArticleLu);
        //}
        /////////////////////////////////////

        $parametres=$blogRepository->findAll();

        //Récupération sous forme d'objet anonyme des 3 articles les plus likès.
        $articles=$articleRepository->find3BestArticleSQL('Publié');
        //dump($articles);

        $articleComments=[];
        $lastComments=$session->get('lastComments',[]);
        foreach ($lastComments as $id) {
            $articleComments[]=$articleRepository->find($id);
        }

        //dump($articleComments);
       
 		return $this->render('blog/home.html.twig',[
            "slogan"=>$parametres[0]->getSlogan(),
            "articles"=>$articles,
            "articleLu"=>$articleLu,
            "articleComments"=>$articleComments,
        ]);
        
    }

     /**
    * @Route("/bo", name=" bo_index")
    * @Security("is_granted('ROLE_ADMIN')")
    */
    public function index(ArticleRepository $articleRepository){
        $articleWithInvalidComment=$articleRepository->findArticleWithInvalidComment();
        dump($articleWithInvalidComment);

        return $this->render("bo/index.html.twig",[ "articleWithInvalidComment"=>$articleWithInvalidComment]);
    }

      /**
    * @Route("/bo/comments", name="bo_comments")
    * @Security("is_granted('ROLE_ADMIN')")
    */
    public function comments(ArticleRepository $articleRepository){

        $articleWithInvalidComment=$articleRepository->findArticleWithInvalidComment();
        return $this->render("bo/comments.html.twig",[ "articleWithInvalidComment"=>$articleWithInvalidComment]);
    
    }

    /**
    * @Route("/article/{etat}/dernier", name="article_dernier")
    */
    public function dernierArticles($etat = 'Publié',ArticleRepository $articleRepository)
    {
       
        $articles=$articleRepository->findLastArticles($etat);

        return $this->render('blog/_recent_articles.html.twig', [
            'articles' => $articles
        ]);
    }
   
 
}
