<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Events\CommentCreatedEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Psr\EventDispatcher\EventDispatcherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommentController extends AbstractController
{
    /**
     * @Route("/comment/{id}/new", name="comment_new")
	 * @IsGranted("ROLE_USER")
     */
    public function new(Article $article,Request $request,EventDispatcherInterface $eventDispatcher,SessionInterface $session)
    {
        $comment=new Comment();
        $form=$this->createForm(CommentType::class,$comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
                     
            $comment->setArticle($article)
                    ->setUser($this->getUser())
                    ->setCode(uniqid())
                    ->setCreatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            $eventDispatcher->dispatch( new CommentCreatedEvent($comment));
            
            $lastComments=$session->get('lastComments',[]);
            if (!in_array($comment->getArticle()->getId(),$lastComments)) {
                $lastComments[]=$comment->getArticle()->getId();
            }
            $session->set('lastComments',$lastComments);
        
        }

        return $this->redirectToRoute('article_show',['id'=>$article->getId()]);

    }

   /**
    * @Route("/comment/{id}/delete/{type?}", name="comment_delete")
	* @Security("is_granted('ROLE_USER') and user===comment.getUser()",message="Vous devez être l'auteur de ce commentaire pour pouvoir le supprimer ")
    */
    public function comment_delete(Comment $comment,$type=''){
          
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($comment);
        $entityManager->flush();

        if ($type=='bouser') {
            return $this->redirectToRoute('user_commentaire',['id'=>$comment->getUser()->getId()]) ;
        }
 
        return $this->redirectToRoute('article_show',['id'=>$comment->getArticle()->getId()]) ;
     }

    /**
    * @Route("/comment/{code}/valide/{type?}", name="comment_valide")
    * @Security("is_granted('ROLE_ADMIN')" )
    */
    public function comment_valide(Comment $comment,$type=''){

          
        $entityManager = $this->getDoctrine()->getManager();
        $comment->setCode('');
        $entityManager->persist($comment);
        $entityManager->flush();

        $messageFlash='Le commentaire a bien été validé ';
        $this->addFlash('success', $messageFlash);

        if ($type=='bo') {
            return $this->redirectToRoute('bo_comments') ;
        }
 
        return $this->redirectToRoute('article_show',['id'=>$comment->getArticle()->getId()]) ;
     }

    /**
    * @Route("/comment/{code}/invalide/{type?}", name="comment_invalide")
    * @Security("is_granted('ROLE_ADMIN')" )
    */
    public function comment_invalide(Comment $comment,$type=''){
          
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($comment);
        $entityManager->flush();

        $messageFlash='Le commentaire a bien été supprimé ';
        $this->addFlash('danger', $messageFlash);

        if ($type=='bo') {
            return $this->redirectToRoute('bo_comments') ;
        }
 
        return $this->redirectToRoute('article_show',['id'=>$comment->getArticle()->getId()]) ;
     }


}
