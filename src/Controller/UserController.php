<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    

     /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte")
     */

    public function show(User $user1): Response
    {

        // $flashbag = $this->get('session')->getFlashBag();
        // dump($flashbag);

        return $this->render('user/show.html.twig', [
            'user' => $user1,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
    * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte")
     */
    public function edit(Request $request, User $user1): Response
    {
        $form = $this->createForm(UserType::class, $user1);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Vos données ont été modidiées');
            return $this->redirectToRoute('user_show',['id'=>$this->getUser()->getId()]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user1,
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/mdpoublie", name="user_mdpoublie")
     */
    public function mdpoublie(Request $request,UserRepository $userRepository, \Swift_Mailer $mailer){
            //Je récupère l'email envoyé par le formulaire
            $email=$request->request->get('email');
            //Je récupère le user qui a cet email
            $user=$userRepository->findOneBy(['email' =>$email]);
            //Si le user existe
            if($user){
                //j'envoie un mail de réinitialisation (fonction mailreinitmdp)
                $affichage=$this->mailreinitmdp($user, $mailer);
                $this->addFlash('success', $affichage);
            }else{
                $affichage="<h4>Cet email n'existe pas</h4>";
                $this->addFlash('danger', $affichage);
            }


            //Je renvoie vers la page de login avec le message correspondant
            return $this->redirectToRoute('security_login');

    }

    /**
    * @Route("/valid/{code}", name="user_valid")
    */
    public function validation($code,UserRepository $userRepository,Request $request,UserPasswordEncoderInterface $encoder){
        

        //je récupère l'utilisateurcorrespondant au code
        $user=$userRepository->findOneBy( ['code' => $code]);
        $message="";


        //Si je reviens dans le controller suite au click sur le bouton submit du formulaire
        if($request->request->count()>0){

           
            //Je récupère les 2 password
            $password=$request->request->get('password');
            $passwordconfirm=$request->request->get('passwordconfirm');

            //Si les passwords sont identiques
            if($password==$passwordconfirm){
                //J'encode le password et je le stocke dans la table
                $hash=$encoder->encodePassword($user,$password);
                $user->setPassword($hash);

               //Je vide le code
              $user->setCode('');

                //Je mets à jour la table
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $message="Votre mot de passe a été réinitialisé avec succès";
                $this->addFlash('success', $message);

                //Je renvoie vers la page de login avec le message de succès
                return $this->redirectToRoute('security_login');
            
            }else{
                //Sinon je renvoie vers la pge de saisie des mots de passe 
                $message="Vos mots de passe ne sont pas identiques , veuillez les saisir à nouveau";
                $this->addFlash('danger', $message);
            }
        
        }
	    //Je renvoie vers la page de login avec le message correspondant
        return $this->render('user/validation.html.twig',[
            "user"=>$user
        ]);

    }

        /**
     * @Route("/{id}/mdpreinit", name="user_mdpreinit")
      * @Security("(is_granted('ROLE_USER') and user===user1)",message="Vous devez être connecté et être le propriétaire du compte")
     */
    public function mdpreinit(User $user1, \Swift_Mailer $mailer)
    {
            //j'envoie un mail de réinitialisation (fonction mailreinitmdp)
            $affichage=$this->mailreinitmdp($user1, $mailer);
            //J'affiche la page d'affichage des infos user avec le message
            return $this->render('user/show.html.twig',[
                'user' => $user1,
                "message"=>$affichage
            ]);

    }

    function mailreinitmdp($user, $mailer){
        //je génère un code 
        $code=uniqid();
        //Que je stocke dans la table
        $user->setCode($code);
        //Récupération du mail 
        $email=$user->getEmail();
        //Je valide la modification
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        //J'envoie un message au user
        $message = new \Swift_Message('REINITIALISATION MOT DE PASSE BLOG');
        $message ->setFrom('admin.blog@email.fr');
        $message ->setTo([$email => $user->getUsername()]);
        //Le contenu du message est stocké dans le fichier _emailreinit.html.twig, auquel j’envoie
        // deux variables : le nom du user et le code de validation
        $message ->setBody(
                $this->renderView(
                    'user/_emailreinit.html.twig',[
                        'name' => $user->getUsername(),
                        'code'=>$code
                    ]
                ),
                'text/html'
        );
        try {
          if($mailer->send($message)) {
             $affiche='<h4>Vous allez recevoir un mail pour réinitialiser votre mot de passe</h4>';
          }else{
             $affiche='<h4>Votre réinitialisation du mot de passe a échoué </h4>';
          }
        }
        catch (\Swift_TransportException $e) {
            //Intercepte l'erreur et renvoie le message EN MODE DEV NE MARCHE PAS
            $affiche= $e->getMessage();
        }
         return $affiche;
     }
 
      /**
     * @Route("/attente/{id}", name="user_attente")
     * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte")
     */
    public function attente(User $user1,UserRepository $userRepository)
    {       
        $articles=$userRepository->findArticleUser('Attente',$user1->getId());
        return $this->render('user/attente.html.twig',['articles'=>$articles]);
    }

      /**
     * @Route("/brouillon/{id}", name="user_brouillon")
     * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte")
     */
    public function brouillon(User $user1,UserRepository $userRepository)
    {
        $articles=$userRepository->findArticleUser('Brouillon',$user1->getId());
        return $this->render('user/brouillon.html.twig',['articles'=>$articles]);
    }

      /**
     * @Route("/commentaire/{id}", name="user_commentaire")
     * @Security("(is_granted('ROLE_USER') and user===user1) or is_granted('ROLE_ADMIN')",message="Vous devez être connecté et être le propriétaire du compte")
     */
    public function commentaire(User $user1,ArticleRepository $articleRepository)
    {
        $articles=$articleRepository->findArticleWithInvalidComment($user1->getId());
        return $this->render('user/commentaire.html.twig',['articles'=>$articles]);
    }


   
}
