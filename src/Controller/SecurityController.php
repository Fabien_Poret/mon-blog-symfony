<?php

namespace App\Controller;

use App\Entity\User;

use App\Util\Fonctions;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request,UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
       
       $mail_admin=Fonctions::getEnv('mail_admin');

        $user = new User();
        $form=$this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            //Génération d'un code au moment de l'inscription
            $code=uniqid();
            //Enegistrement du code
            $user->setCode($code);
            //Récupérationdu mail saisi
            $email=$user->getEmail();
            //Ajout d'une chaîne aléatoire de 10 caractères au bout de l'email
            $user->setEmail($email.$this->generateRandomString());
            //Code ajout user
            $hash=$encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($hash);
            $user->setRoles(["ROLE_USER"]);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            //Code envoi message avec email non modifié
            $message = new \Swift_Message('CONFIRMATION INSCRIPTION BLOG');
            $message ->setFrom($mail_admin);
            $message ->setTo([$email => $user->getUsername()]);
            $message ->setBody(
                    $this->renderView(
                        'security/_emailregistration.html.twig',[
                            'name' => $user->getUsername(),
                            'code'=>$code
                        ]
                    ),
                    'text/html'
            );
            //Envoi du message
            try {
                if($mailer->send($message)){
                    $affiche=1;                  
                }else{
                    $affiche=2;
                }
            }
            catch (\Swift_TransportException $e) {               
                //Intercepte l'erreur et renvoie le message EN MODE PROD MARCHE PAS
                $affiche= $e->getMessage();
                //supression de l'utilisateur si échec envoi mail
                $entityManager->remove($user);
                $entityManager->flush();
            }     
            //dump($affiche);
            return $this->render('security/login.html.twig',[
                "message"=>$affiche
            ]);
        }

        return $this->render('security/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(Request $request)
    {
        $message = $request->query->get('message', '');              
        return $this->render('security/login.html.twig',[
           'message'=>$message
        ]);

    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout(){  }

/**
* @Route("/validation/{code}", name="security_validation")
*/
public function validation($code,UserRepository $userRepository,Request $request)
{
    $user=$userRepository->findOneBy( ['code' => $code]);
    if($user){
        $message=1;
        //On supprime les caractères générés au bout de l'email
        $email=substr($user->getEmail(),0,strlen($user->getEmail())-10);
        //E on le réécrit dans la table
        $user->setEmail($email);
        //On vide le code qui avait servi à la validation
        $user->setCode('');
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }else{
        $message=0;
    }
    return $this->render('security/validation.html.twig',["message"=>$message]);

}

    //Fonction qui génére une chaîne de caractères aléatoire
    /**
    * @return string
    */
    function generateRandomString($longueur = 10)
    {
        $caracteres = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_/|*-+';
        $longueurMax = strlen($caracteres);
        $chaineAleatoire = '';
        //Si $longueur n'est pas passé en paramètre, il vaut  par défaut 
        for ($i = 0; $i < $longueur; $i++){
            $chaineAleatoire .= $caracteres[rand(0, $longueurMax - 1)];
        }
        return $chaineAleatoire;
    }



}
