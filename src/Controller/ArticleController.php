<?php

namespace App\Controller;

use App\Entity\Alike;
use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Service\FileUploader;
use App\Events\ArticleEditEvent;
use App\Repository\EtatRepository;
use App\Repository\AlikeRepository;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Serializer\Encoder\JsonEncode;

class ArticleController extends AbstractController
{
    /**
    * @Route("/article", name="article_index")
    */
    public function index(ArticleRepository $articleRepository,EtatRepository $etatRepository  )
    {

        // $articles = $articleRepository->findArticleQB('Publié');
        // dump($articles);
        // $articles = $articleRepository->findArticleDQL('Publié');
        // dump($articles);
        //$articles = $articleRepository->findArticleSQLNATIVE('Publié');
        //dump($articles);

      
        if($this->isGranted('ROLE_ADMIN')){
            $articles = $articleRepository->findArticleSQLNATIVE('Attente');
        }else{
            $articles = $articleRepository->findArticleSQLNATIVE('Publié');
        }

        return $this->render('article/index.html.twig', [
            'articles' => $articles 
        ]);
    }

    /**
     * @Route("/article/{id}/show", name="article_show")
     * @security("article.getEtat().getStatut()=='Publié' or is_granted('ROLE_ADMIN')",message="Vous ne pouvez pas afficher un article en attente ou brouillon")
     */
    public function show(Article $article){

        $comment=new Comment();
        $formComment=$this->createForm(CommentType::class,$comment);

        /////////Envoi d'un cookie//////////NE FONCTIONNE PAS CHEZ TOUS LES HEBERGEURS
        //if($this->getUser()){
        //    $cookie=new Cookie('articleLu_'.$this->getUser()->getId(),$article->getId(),time() + ( 2 * 365 * 24 * 60 * 60)	);
        //    $response=new Response();
        //    $response->headers->setCookie($cookie);
        //    $response->send();
        //}
        /////////////////////////////////////

        return $this->render("article/show.html.twig",[
            'article'=>$article,
            'formComment'=>$formComment->createView()
        ]);

    }


    /**
    *  @Route("/article/new", name="article_new")
    * @Security("is_granted('ROLE_USER')")
    */
    public function new(Request $request,EtatRepository $etatRepository,CategoryRepository $categoryRepository,FileUploader $fileUploader){

        $article=new Article();

        $form=$this->createForm(ArticleType::class,$article,[
            'currentUser'=>$this->getUser()
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted()&& $form->isValid()){
            ///////////////CODE UPLOAD//////////////
            $file=$form['fichierimage']->getData();           
            if($file){
                $res=$fileUploader->upload($file);
                if(is_string($res)){
                    $article->setImage($res);
                }else{
                    $message=$res->getMessage();
                    $article->setImage('');
                }
            }else{
                $article->setImage(''); 
            }
            
            ///////////////////////////////////////

            $category=$categoryRepository->find($form['category']->getData());       
            $article->setCategory($category);

            $article->setCreatedAt(new \DateTime())
                    ->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($article);
            $entityManager ->flush();

            //$this->mailNewArticle($article,$mailer);

            return $this->redirectToRoute('article_index');

        }

        return $this->render('article/new.html.twig',[    
            'formArticle'=>$form->createView()
        ]);

        
    }

    /**
    * @Route("/article/{id}/edit/{type?}", name="article_edit")
    * @Security("is_granted('ROLE_USER') and user===article.getUser() or is_granted('ROLE_ADMIN')",message="Vous devez être l'auteur de cette article pour pouvoir le modifier ")
    */
    public function edit(Request $request,Article $article,$type='',EtatRepository $etatRepository,CategoryRepository $categoryRepository,FileUploader $fileUploader,EventDispatcherInterface $eventDispatcher){

        $form=$this->createForm(ArticleType::class,$article,[
            'currentUser'=>$this->getUser()
        ]);

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            ///////////////CODE UPLOAD//////////////
            $file=$form['fichierimage']->getData();           
            if($file){
                $res=$fileUploader->upload($file);
                if(is_string($res)){
                    $article->setImage($res);
                }else{
                    $message=$res->getMessage();
                    $article->setImage('');
                }
            }else{
                $article->setImage($article->getImage()); 
            }

            ///////////////////////////////////////
            
            $category=$categoryRepository->find($form['category']->getData());       
            $article->setCategory($category);

            $article->setModifiedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager ->persist($article);
            $entityManager ->flush();

            $eventDispatcher->dispatch( new ArticleEditEvent($article));

            if ($type=="attente") {
                return $this->redirectToRoute('user_attente',['id'=>$article->getUser()->getId()]);
            }

            if ($type=="brouillon") {
                return $this->redirectToRoute('user_brouillon',['id'=>$article->getUser()->getId()]);
            }

            return $this->redirectToRoute('article_index');

        }

        return $this->render('article/edit.html.twig',[
            'formArticle'=>$form->createView(),
            'article' => $article
        ]);       
    }

     /**
     * @Route("/article/{id}/delete/{type?}", name="article_delete")
     * @Security("is_granted('ROLE_USER') and user===article.getUser()  or is_granted('ROLE_ADMIN')", statusCode=499,message="Vous devez être connecté et être l'auteur de cet article pour le supprimer")
     */
    public function delete(Request $request, Article $article, $type='')
    {
     
        $entityManager = $this->getDoctrine()->getManager();

        //Supression des commentaires des articles
        foreach($article->getComments() as $comment){
            $entityManager->remove($comment);
        }

        //Supression des likes des articles
        foreach($article->getAlikes() as $like){
            $entityManager->remove($like);
        }

        //Supression de l'image des articles
        if(file_exists("images/articles/".$article->getImage() &&  $article->getImage()!='' )){
            unlink("images/articles/".$article->getImage());
        } 

        $entityManager->remove($article);
        $entityManager->flush();

        if ($type=="attente") {
            return $this->redirectToRoute('user_attente',['id'=>$article->getUser()->getId()]);
        }

        if ($type=="brouillon") {
            return $this->redirectToRoute('user_brouillon',['id'=>$article->getUser()->getId()]);
        }
   
        return $this->redirectToRoute('article_index');
        
    }

     /**
     * @Route("/article/{id}/like", name="article_like")
     */
    public function like(Article $article, AlikeRepository $alikeRepository){

        $entityManager = $this->getDoctrine()->getManager();

        $user=$this->getUser();

        if (!$user) {
            return $this->json(["code"=>403,"message"=>'Vous devez vous connecter pour aimer'],403);
        }

        if($article->isLikedByUser($user)){
            $like=$alikeRepository->findOneBy(
                [
                    'article'=>$article,
                    'user'=>$user
                ]
            );

            $entityManager->remove($like);
            $entityManager->flush();

            return $this->json([
                "code"=>200,
                "message"=>'Like Bien supprimé',
                "likes"=>$alikeRepository->count(['article'=>$article])],
                200
            );

        }


        $like=new Alike();
        $like->setArticle($article);
        $like->setUser($user);
        $entityManager->persist($like);
        $entityManager->flush();
        
        return $this->json([
            "code"=>200,
            "message"=>'Like Bien ajouté',
            "likes"=>$alikeRepository->count(['article'=>$article])],
            200
        );


    }

     /**
     * @Route("/article/{id}/publier/{type?}", name="article_publier")
     * @Security("is_granted('ROLE_ADMIN')" ,message="Vous devez être administrateur pour publier cet article")
     */
    public function publier(Article $article,$type="", EtatRepository $etatRepository){
        
        
        $entityManager = $this->getDoctrine()->getManager();
        $article->setEtat($etatRepository->findOneBy(['statut'=>'Publié']));
        $entityManager->persist($article);
        $entityManager->flush();
        
        if($type=="json"){
            return $this->json([
                "code"=>200,
                'id'=>$article->getId(),
                "message"=>'Article publié'
            ],
                200
            );
        }

        return $this->redirectToRoute('article_index');
        

    }

    private function mailNewArticle(Article $article, $mailer){

        if($article->getEtat()->getStatut()=='Attente'){
   
           $href = $this->generateUrl('article_show', [
             'id' => $article->getId(),
           ], UrlGeneratorInterface::ABSOLUTE_URL);
   
           $lien='<br><a href="'.$href.'">Afficher Article</a>';
           //Envoi du message
           $message = new \Swift_Message('NOUVEL ARTICLE');
           $message ->setFrom('admin.blog@email.fr');
           $message ->setTo(['admin.blog@email.fr'=> 'admin']);
           $message ->setBody("
                       Un Nouvel article a été créé 
                       <br>Titre de l'article <strong>".$article->getTitle()."</strong>
                       <br>par <strong>".$article->getUser()."</strong>
                       <br>Lien vers l'article : 
                       ".$lien,
                   'text/html'
           );
           try {
               $retour=$mailer->send($message);
           }
           catch (\Swift_TransportException $e) {
               $retour= $e->getMessage();
           }
       }
   }

    /**
     * @Route("/article/{id}/download", name="article_download")
     * @security("is_granted('ROLE_USER')",message="Vous devez être connecté pour télécharger une image")
     */
    public function download(Article $article){

        $image=$article->getImage();

        //Télécharge un fichier contenu dans votre site
        return $this->file('images/articles/'.$image);
        //return $this->file('images/articles/'.$image, 'monimage.jpg');
        //return $this->file('images/articles/'.$image, 'monimage.jpg', ResponseHeaderBag::DISPOSITION_INLINE);
        //Télécharge un fichier contenu dans votre système de fichier
        //$file = new File('c://images/IMG-1391.jpg');
        //return $this->file($file);
        //return $this->file($file, 'custom_name1.pdf');
        //return $this->file($file, 'custom_name2.pdf', ResponseHeaderBag::DISPOSITION_INLINE);

    }

     /**
     * @Route("/article/title/autocomplete", name="article_title_autocomplete")
     */
    public function title_autocomplete(ArticleRepository $articleRepository){
        $titles=$articleRepository->findAutoCompleteTitle();
        return $this->json(json_encode($titles));
    }

    
     /**
     * @Route("/article/category/autocomplete", name="article_category_autocomplete")
     */
    public function category_autocomplete(CategoryRepository $categoryRepository){
        $categories=$categoryRepository->findAutoCompleteCategory();
        return $this->json(json_encode($categories));
    }


   
}
