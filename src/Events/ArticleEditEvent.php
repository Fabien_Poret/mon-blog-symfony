<?php

namespace App\Events;
use App\Entity\Article;

use Symfony\Contracts\EventDispatcher\Event;
 
class ArticleEditEvent extends Event
{
    protected $article;
 
    public function __construct(Article $article)
    {
        $this->article = $article;
    }
    public function getArticle()
    {
        return $this->article;
    }
}
